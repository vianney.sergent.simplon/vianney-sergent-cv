<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CVController extends AbstractController
{
    /**
     * @Route("/c/v", name="c_v")
     */
    public function index()
    {
        return $this->render('cv/index.html.twig', [
            'controller_name' => 'CVController',
        ]);
    }

    /**
     * @Route("/", name="Accueil")
     */
    public function accueil()
    {
        return $this->render('cv/acceuil.html.twig');
    }

    /**
     * @Route("/cv/competence", name="Competence")
     */
    public function competence()
    {
        return $this->render('cv/competence.html.twig');
    }

    /**
     * @Route("/cv/experience", name="Experience")
     */
    public function experience()
    {
        return $this->render('cv/exeperience.html.twig');
    }

    /**
     * @Route("/cv/formation", name="Formations")
     */
    public function formation()
    {
        return $this->render('cv/formation.html.twig');
    }
     /**
     * @Route("/cv/formation", name="Formations")
     */
    public function sujet()
    {
        return $this->render('cv/sujet.html.twig');
    }
     /**
     * @Route("/cv/formation", name="Formations")
     */
    public function contact()
    {
        return $this->render('cv/contact.html.twig');
    }
}
